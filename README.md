# Booking Framework Test
Booking.com ofrece un servicio de reserva online mediante el cual los proveedores de alojamiento (entre otros), como los hoteleros y otros proveedores, pueden ofrecer sus productos y servicios de reserva para que los usuarios de la Plataforma puedan usarlos para reservar.
Sitio web: https://www.booking.com/

## Librerías usadas

* Serenity BDD
* Cucumber
* Junit
* Maven
* Logger: slf4j
* Patrón de diseño: Screenplay

## Correr tests:


### Correr todos los tests:
    mvn clean verify

### Correr en un ambiente: 
Para ver las diferentes opciones, revisar archivo serenity.conf (src/test/resources/serenity.conf)
    
    mvn clean verify -Dwebdriver.driver=chrome -Denvironment=prod

### Correr por tags:
    mvn verify -Dcucumber.options="--tags @stays"

### Informes
Al corer las pruebas se genera un informe automáticamente, sin embagro hay algunas casos de pruebas que son unos "Flakey Test" (pruebas que en algunas ocaciones pasan y en otras fallan). En caso de fallo correr el siguiente comando para ver el informe detallado.
   
     mvn serenity:aggregate

Nota: algunas pruebas fallan debido a la naturaleza de la página, la cual en ocaciones limita la interacción de pruebas automatizadas.

## Casos de prueba
https://drive.google.com/drive/folders/19c7IVsaBr77HGMeGZaSenJSezGoI8Is-?usp=sharing

