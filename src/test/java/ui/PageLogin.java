package ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PageLogin {

    public static final Target BUTTON_GO_TO_LOGIN = Target.the("Button to go login form")
            .located(By.xpath("(//span[contains(text(),'Inicia sesión')])[1]"));

    public static final Target EMAIL = Target.the("Username input")
            .located(By.id("username"));

    public static final Target PASSWORD = Target.the("Password input")
            .located(By.id("password"));

    public static final Target BUTTON_CONTINUE = Target.the("Button to continue with password")
            .located(By.xpath("//span[contains(text(),'Continuar con e-mail')]"));

    public static final Target BUTTON_LOGIN = Target.the("Boton de sgn in")
            .located(By.xpath("//span[contains(text(),'Iniciar sesión')]"));


}
