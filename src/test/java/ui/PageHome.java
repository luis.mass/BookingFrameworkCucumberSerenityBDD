package ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PageHome {
    public static final Target WELCOME_MESSAGE_SPANISH = Target.the("Message in spanish")
            .located(By.xpath("//span[contains(text(),'Busca ofertas en hoteles, casas y mucho más')]"));

    public static final Target WELCOME_MESSAGE_ENGLISH = Target.the("Message in english")
            .located(By.xpath("//span[contains(text(),'Find deals on hotels, homes and much more...')]"));

    public static final Target WELCOME_MESSAGE_ITALIAN = Target.the("Message in italian")
            .located(By.xpath("//span[contains(text(),'Trova offerte su hotel, case e tanto altro...')]"));

    public static final Target USERNAME_LOGIN = Target.the("Name of user")
            .located(By.xpath("//span[contains(text(),'Luis')]"));

    public static final Target USERNAME_LOGIN_INCORRECT = Target.the("Name of user")
            .located(By.cssSelector("#password-note"));

    public static By USERNAME_LOGIN_VALIDATION = By.cssSelector("#profile-menu-trigger--title");
}
