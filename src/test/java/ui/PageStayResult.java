package ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class PageStayResult extends PageObject {

    public static final Target TITLE_RESULT = Target.the("Title of the page stay results")
            .located(By.xpath("//*[@id='right']/div[1]/div/div/div/h1"));

    public static Target LIST_STAYS = Target.the("List of Stays")
            .located(By.xpath("//div/div[1]/div[2]/div/div[1]/div/div[1]/div/div[2]/div/a/span/span[1]"));
}

