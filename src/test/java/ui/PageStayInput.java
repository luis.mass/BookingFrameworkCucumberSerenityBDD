package ui;


import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class PageStayInput {

    public PageStayInput() {
    }

    public static final Target ICON_LANGUAJE_HOME = Target.the("Input of destination")
            .located(By.xpath("//span/div/img[@class='bui-avatar__image']"));

    public static final Target DESTINATION = Target.the("Input of destination")
            .located(By.cssSelector("#ss"));

    public static final Target BUTTON_SEARCH = Target.the("Button search")
            .located(By.xpath("//span[contains(text(),'Buscar')]"));

    public static Target ICON_LANGUAJE;

    public static final Target WELCOME_MESSAGE = Target.the("Button search")
            .located(By.xpath("//span[contains(text(),'Busca ofertas en hoteles, casas y mucho más')]"));

    public static Target findLanguaje(String languaje) {
        return ICON_LANGUAJE = Target.the("Button search")
                .located(By.xpath("(//div[contains(text(),'"+languaje+"')])[1]"));
    }


}