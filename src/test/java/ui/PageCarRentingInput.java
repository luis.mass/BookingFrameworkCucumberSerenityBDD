package ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PageCarRentingInput {
    public static final Target DESTINATION = Target.the("Input of destination")
            .located(By.cssSelector("#ss_origin"));

    public static final Target BUTTON_SEARCH = Target.the("Button to search cars")
            .located(By.xpath("//span[contains(text(),'Buscar')]"));

    public static final Target FIRST_DESTINATION = Target.the("Button to search cars")
            .located(By.xpath("//*[@id='frm']/div[2]/div[1]/div/div[1]/ul[1]/li[1]"));
}

