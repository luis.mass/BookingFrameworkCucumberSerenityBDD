package ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class PageCarRentingResult {

    public static final Target CATEGORY_CAR_RENTALS = Target.the("Button of category car rentals")
            .located(By.xpath(" //span[contains(text(),'Alquiler de coches')]"));

    public static final Target SELECT_SORT = Target.the("Input to select the sort order")
            .located(By.cssSelector("#search-results-filter-selection-desktop"));

    public static final Target TITLE_FIRST_CAR = Target.the("Title of the first car")
            .located(By.xpath("/html/body/div[2]/div[2]/div/div[2]/div[5]/div[1]/section/div/div[1]/div[2]/div/div[1]/div/div/div[1]/div/div[2]/div/h3"));


}


