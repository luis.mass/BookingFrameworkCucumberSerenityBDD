package stepDefinitions;


import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import questions.ThePageHome;
import tasks.Choose;
import tasks.Decided;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.containsString;


public class SelectLanguajeStepDefinitions {

    String actor;
    @Dado("^que (.*) ingresa al sitio web de Booking$")
    public void on_the_booking_home_page(String actor) {
        this.actor=actor;
        theActorCalled(actor).has(Decided.goToBookingSite());
    }

    @Cuando("^el configura el idioma en: (.*)$")
    public void set_languaje(String Languaje) {
        theActorInTheSpotlight().attemptsTo(
                Choose.languaje(Languaje)
        );
    }

    @Entonces("^el debería ver el contenido de la página en español$")
    public void content_is_displayed_in_spanish() {
        theActorInTheSpotlight().should(
                seeThat("Title of booking home",
                        ThePageHome.welcomeMessageInSpanish(),
                        containsString("Busca ofertas en hoteles, casas y mucho más"))
        );
    }

    @Entonces("^el debería ver el contenido de la página en inglés$")
    public void content_is_displayed_in_english() {
        theActorInTheSpotlight().should(
                seeThat("Title of booking home",
                        ThePageHome.welcomeMessageInEnglish(),
                        containsString("Find deals on hotels, homes and much more..."))
        );
    }

    @Entonces("^el debería ver el contenido de la página en italiano$")
    public void content_is_displayed_in_italian() {
        theActorInTheSpotlight().should(
                seeThat("Title of booking home",
                        ThePageHome.welcomeMessageInItalian(),
                        containsString("Trova offerte su hotel, case e tanto altro..."))
        );
    }
}
