package stepDefinitions;


import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import tasks.Choose;
import tasks.Decided;
import tasks.TypeDestinationStay;
import ui.PageStayResult;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.*;

@Slf4j
public class StaysStepDefinitions {

    PageStayResult pageStayResult;
    String actor;


    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^que el (.*) entra a la sección de alojamientos de Booking$")
    public void go_to_booking_site(String actor) {
        //BrowseTheWeb.as(Actor.named(actor)).setImplicitTimeout(15, SECONDS);
        theActorCalled(actor).has(Decided.goToBookingSite());
    }

    @Cuando("^El realiza la búsqueda de: (.*)$")
    public void search_by_key_word(String keyWord) {
        theActorInTheSpotlight().attemptsTo(
                TypeDestinationStay.destination(keyWord)
        );
    }

    /*@Entonces("^El solo debería poder ver los resultados relacionados con la palabra: (.*)$")
    public void should_look_results_with_the_word(String keyWordWaited) {
        theActorInTheSpotlight().should(
                seeThat("Title of booking home",
                        ThePageStayResult.welcomeMessage(),
                        containsString(keyWordWaited))
        );
    }*/

    @Entonces("^El solo debería poder ver los resultados relacionados con la palabra: (.*)$")
    public void should_look_results_with_the_word(String keyWordWaited) {
        theActorInTheSpotlight().should(
                seeThat("Title of booking home",
                        TheTarget.textValuesOf(PageStayResult.LIST_STAYS),
                        hasItems(keyWordWaited))
        );
    }


    @Dado("^que (.*) tiene configurado en la plataforma el idioma español$")
    public void set_languaje(String actor) {
        this.actor = actor;
        theActorCalled(actor).has(Decided.goToBookingSite());
        theActorInTheSpotlight().attemptsTo(
                Choose.languaje("Español")
        );
    }
}

//GetListStays.ofStays()