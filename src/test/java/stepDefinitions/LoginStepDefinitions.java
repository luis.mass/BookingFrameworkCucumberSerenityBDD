package stepDefinitions;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Pero;
import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.screenplay.waits.WaitUntil;
import questions.ThePageHome;
import tasks.Choose;
import tasks.Decided;
import tasks.DoLoginWith;
import ui.PageHome;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.hamcrest.Matchers.containsString;

@Slf4j
public class LoginStepDefinitions {
    String actor;

    @Dado("^que (.*) tiene una cuenta activa$")
    public void has_an_active_account(String actor) {
        this.actor = actor;
        theActorCalled(actor).has(Decided.goToBookingSite());
        theActorInTheSpotlight().attemptsTo(
                Choose.languaje("Español")
        );
    }

    @Cuando("^el envía sus credenciales correctas$")
    public void he_send_his_valid_credentials() {
        theActorInTheSpotlight().attemptsTo(
                DoLoginWith.username("lmass24@hotmail.com").password("Test_2021#"),
                WaitUntil.the(PageHome.USERNAME_LOGIN_VALIDATION, isVisible()).forNoMoreThan(10).seconds()
        );
    }

    @Entonces("^el debería tener acceso al panel principal")
    public void should_look_admin_panel() {
        theActorInTheSpotlight().should(
                seeThat("El nombre de usaurio se muestra en la página principal",
                        ThePageHome.usernameLogin(),
                        containsString(this.actor))
        );
    }


    @Cuando("el envía sus credenciales incorrectas")
    public void he_send_his_invalid_credentials() {
        theActorInTheSpotlight().attemptsTo(
                DoLoginWith.username("lmass24@hotmail.com").password("1234"),
                WaitUntil.the(PageHome.USERNAME_LOGIN_VALIDATION, isVisible()).forNoMoreThan(10).seconds()
        );
    }

    @Pero("el no debería tener acceso al panel principal")
    public void incorrect_access() {
        theActorInTheSpotlight().should(
                seeThat("usuario o contraseña incorrectas",
                        ThePageHome.usernameLoginIncorrect(),
                        containsString("La combinación de e-mail y contraseña que has introducido no coinciden"))
        );
    }
}
