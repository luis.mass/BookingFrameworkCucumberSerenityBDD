package stepDefinitions;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import net.serenitybdd.screenplay.waits.WaitUntil;
import questions.ThePageCarRentingResults;
import tasks.*;
import ui.PageCarRentingInput;
import ui.PageCarRentingResult;


import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.hamcrest.Matchers.containsString;

public class CarRentalsStepDefinitions {

    private String actor;

    @Dado("^que (.*) va a la sección de alquiler de autos de Booking$")
    public void he_navigates_to_rental_cars_site_booking(String actor) {
        this.actor = actor;
        theActorCalled(actor).has(Decided.goToBookingSite());
        theActorCalled(actor).attemptsTo(
                Choose.languaje("Español"),
                GoToCategory.carRenting()
        );
    }

    @Cuando("^El realiza la búsqueda de autos disponibles en: (.*)$")
    public void search_cars(String keyWord) {
        theActorInTheSpotlight().attemptsTo(
                TypeDestinationCarRenting.destination(keyWord),
                WaitUntil.the(PageCarRentingResult.TITLE_FIRST_CAR, isVisible()).forNoMoreThan(10).seconds()
        );
    }

    @Y("^ordena los autos de menor a mayor precio$")
    public void sort_car_prices() {
        theActorInTheSpotlight().attemptsTo(
                Sort.orderBy("De menos a más"),
                WaitUntil.the(PageCarRentingResult.TITLE_FIRST_CAR, isVisible()).forNoMoreThan(10).seconds()
        );
    }

    @Entonces("^debería ver que el auto de menor precio es un (.*)$")
    public void should_look_the_lowest_price(String car) {
        theActorInTheSpotlight().should(
                seeThat("El auto con menor precio",
                        ThePageCarRentingResults.firstTitleCar(),
                        containsString(car))
        );
    }

}
