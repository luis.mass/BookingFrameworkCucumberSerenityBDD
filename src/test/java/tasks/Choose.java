package tasks;

import ui.PageStayInput;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

public class Choose implements Task {

    private final String languaje;

    public Choose(String languaje) {
        this.languaje = languaje;
    }

    public static Performable languaje(String languaje) {
        return Instrumented
                .instanceOf(Choose.class)
                .withProperties(languaje);
    }

    @Override
    @Step("{0} ha seleccionado el lenguaje: #languaje")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PageStayInput.ICON_LANGUAJE_HOME),
                Click.on(PageStayInput.findLanguaje(languaje))
        );
    }
}
