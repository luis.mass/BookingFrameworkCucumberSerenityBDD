package tasks;

import ui.PageCarRentingResult;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Step;

public class GoToCategory implements Task{

        private Target categoryTarget;
        private static String categoryChoosed;

        public GoToCategory(Target category) {
            this.categoryTarget = category;
        }

        public static Performable carRenting() {
            categoryChoosed = "Car rentals";
            return Instrumented.
                    instanceOf(GoToCategory.class)
                    .withProperties(PageCarRentingResult.CATEGORY_CAR_RENTALS);
        }

        @Override
        @Step("{0} decides go to the category: #categoryChoosed")
        public <T extends Actor> void performAs(T actor) {
            actor.attemptsTo(Click.on(this.categoryTarget));
        }
}




