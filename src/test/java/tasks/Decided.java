package tasks;

import constants.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.thucydides.core.annotations.Step;
import static net.serenitybdd.screenplay.Tasks.instrumented;


public class Decided implements Task {

    public Decided(){

    }

    public static Performable goToBookingSite() {
        return instrumented(Decided.class);
    }


    @Override
    @Step("{0} ha decidido ir al Sitio web de Booking")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Navigate.to(Constants.HOME_PAGE)
        );
    }
}
