package tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

public class Navigate implements Interaction {

    @Override
    @Step("{0} navigates to someone page")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.url(URL_OPTION));
    }

    public static Performable to(String URLOption) {
        return Instrumented.instanceOf(Navigate.class)
                .withProperties(URLOption);
    }

    private final String URL_OPTION;

    public Navigate(String URL_OPTION){
        this.URL_OPTION = URL_OPTION;
    }


}
