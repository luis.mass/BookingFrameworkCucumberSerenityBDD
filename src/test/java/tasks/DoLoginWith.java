package tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import ui.PageLogin;


public class DoLoginWith implements Task {
    private final String username;
    private final String password;

    public DoLoginWith(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static LoginWithBuilder  username(String username) {
        return new LoginWithBuilder(username);
    }


    @Override
    @Step("{0} Ingresa usuario: #username y contraseña: #password")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PageLogin.BUTTON_GO_TO_LOGIN),
                Enter.theValue(username).into(PageLogin.EMAIL),
                Click.on(PageLogin.BUTTON_CONTINUE),
                Enter.theValue(password).into(PageLogin.PASSWORD),
                Click.on(PageLogin.BUTTON_LOGIN)
        );
    }

    public static class LoginWithBuilder {
        private String username;

        public LoginWithBuilder(String username) {
            this.username = username;
        }

        public Performable password(String password) {
            return Instrumented.instanceOf(DoLoginWith.class)
                    .withProperties(username,password);
        }
    }

}
