package tasks;

import ui.PageStayInput;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

public class TypeDestinationStay implements Task {

    private final String destination;

    public TypeDestinationStay(String destination) {
        this.destination = destination;
    }

    public static Performable destination(String destination) {
        return Instrumented.
                instanceOf(TypeDestinationStay.class)
                .withProperties(destination);
    }

    @Override
    @Step("{0} type destination: #destination")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(destination).into(PageStayInput.DESTINATION),
                Click.on(PageStayInput.BUTTON_SEARCH)
        );
    }










}
