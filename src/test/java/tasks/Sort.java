package tasks;

import ui.PageCarRentingResult;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.thucydides.core.annotations.Step;

public class Sort implements Task {

    private String critery;

    public Sort(String critery) {
        this.critery = critery;
    }

    public static Performable orderBy(String critery) {
        return Instrumented
                .instanceOf(Sort.class)
                .withProperties(critery);
    }

    //Recommended

    @Override
    @Step("{0} sort de results of elements with the criteria: #critery")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                SelectFromOptions.byVisibleText(critery).from(PageCarRentingResult.SELECT_SORT)
        );
    }
}
