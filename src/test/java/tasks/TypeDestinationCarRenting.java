package tasks;

import ui.PageCarRentingInput;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class TypeDestinationCarRenting implements Task {

    private final String destination;

    public TypeDestinationCarRenting(String destination) {
        this.destination = destination;
    }

    public static Performable destination(String destination) {
        return Instrumented.
                instanceOf(TypeDestinationCarRenting.class)
                .withProperties(destination);
    }


    @Override
    @Step("{0} type destination: #destination")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(destination).into(PageCarRentingInput.DESTINATION),
                WaitUntil.the(PageCarRentingInput.FIRST_DESTINATION, isVisible()).forNoMoreThan(10).seconds(),
                Hit.the(Keys.ARROW_DOWN).into(PageCarRentingInput.DESTINATION),
                Hit.the(Keys.ENTER).into(PageCarRentingInput.DESTINATION),
                Click.on(PageCarRentingInput.BUTTON_SEARCH)
        );
    }










}
