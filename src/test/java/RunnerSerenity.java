import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import ui.PageCarRentingInput;

import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.containsText;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

/**
 * This class is presented only to validate the functionality of the questions in Serenity,
 * the Screenplay pattern was not applied here
 */
@Slf4j
@RunWith(SerenityRunner.class)
public class RunnerSerenity {

    Actor actor;
    private static final String GO_TO_CAR_RENTAL = "//span[contains(text(),'Car rentals')]";

    private static final Target CHECK_IN_MONTH = Target.the("Input to month check in")
            .located(By.xpath("//*[@id='frm']/div[2]/div[3]/div/div[2]/div/div/div/div[1]/div/div[1]/input[1]"));

    private static final Target CHECK_OUT_MONTH = Target.the("Input to month check out")
            .located(By.xpath("//*[@id='frm']/div[2]/div[3]/div/div[3]/div/div[2]/div/div[1]/div/div[1]/input[1]"));

    private static final Target CHECK_IN_DAY = Target.the("Input to day check in")
            .located(By.xpath("//*[@id='frm']/div[2]/div[3]/div/div[2]/div/div/div/div[1]/div/div[1]/input[2]"));

    private static final Target CHECK_OUT_DAY = Target.the("Input to day check out")
            .located(By.xpath("//*[@id='frm']/div[2]/div[3]/div/div[3]/div/div[2]/div/div[1]/div/div[1]/input[2]"));

    private static final Target CHECK_IN_YEAR = Target.the("Input to year check in")
            .located(By.xpath("//*[@id='frm']/div[2]/div[3]/div/div[2]/div/div/div/div[1]/div/div[1]/input[3]"));

    private static final Target CHECK_OUT_YEAR = Target.the("Input to year check out")
            .located(By.xpath("//*[@id='frm']/div[2]/div[3]/div/div[3]/div/div[2]/div/div[1]/div/div[1]/input[3]"));

    private static final Target DESTINITY = Target.the("Input to destinity")
            .located(By.cssSelector("#ss_origin"));

    private static final Target BUTTON_SEARCH =Target.the("Button search")
            .located(By.xpath("//span[contains(text(),'Search')]"));

    private static final Target FIRST_DESTINATION_ON_LIST = Target.the("first destination")
            .located(By.xpath("//*[@id='partner-booking-cars']/div[3]/div[5]/div[1]/div/div[1]/h2"));

    @Managed
    WebDriver driver;

    @Before
    public void setStage() {
        actor = Actor.named("Luis");
        actor.can(BrowseTheWeb.with(driver));
        log.info(System.getProperty("webdriver.driver"));


    }

    @Test
    public void performQuestionToElements() {
        String browser = System.getProperty("webdriver.driver");
        log.info("Browser: --->" + browser);
        actor.attemptsTo(Open.url("https://www.booking.com/"));

        if(getDriver().findElement(By.xpath(GO_TO_CAR_RENTAL)).isDisplayed()) {
            actor.attemptsTo(Click.on(GO_TO_CAR_RENTAL));
        }

        actor.attemptsTo(
                Enter.theValue("Bogotá").into(DESTINITY),
                WaitUntil.the(PageCarRentingInput.FIRST_DESTINATION, isVisible()).forNoMoreThan(10).seconds(),
                Hit.the(Keys.ARROW_DOWN).into(DESTINITY),
                Hit.the(Keys.ENTER).into(DESTINITY),
                Hit.the(Keys.TAB).into(DESTINITY),
                Hit.the(Keys.TAB).into(DESTINITY),
                Enter.theValue("11").into(CHECK_IN_MONTH),
                Enter.theValue("19").into(CHECK_IN_DAY),
                Enter.theValue("2021").into(CHECK_IN_YEAR).thenHit(Keys.ARROW_DOWN).thenHit(Keys.ENTER),
                Enter.theValue("11").into(CHECK_OUT_MONTH),
                Enter.theValue("19").into(CHECK_OUT_DAY),
                Enter.theValue("2021").into(CHECK_OUT_YEAR),
                Click.on(BUTTON_SEARCH)
        );

        /*actor.should(
                seeThat(WebElementQuestion.the(FIRST_DESTINATION_ON_LIST), containsText("Medellín"))
        );*/

        actor.should(
                eventually(seeThat(the(FIRST_DESTINATION_ON_LIST), containsText("Bogota"))).waitingForNoLongerThan(10).seconds()
        );



    }

}
