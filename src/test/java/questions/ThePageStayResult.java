package questions;

import ui.PageStayInput;
import ui.PageStayResult;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.targets.TheTarget;


public class ThePageStayResult {

    public static Question<String> welcomeMessage() {
        return TheTarget.textOf(PageStayResult.TITLE_RESULT);
    }

}
