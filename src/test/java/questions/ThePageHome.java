package questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import org.openqa.selenium.By;
import ui.PageHome;
import ui.PageStayInput;

public class ThePageHome {
    public static Question<String> welcomeMessageInSpanish() {
        return TheTarget.textOf(PageHome.WELCOME_MESSAGE_SPANISH);
    }

    public static Question<String> welcomeMessageInEnglish() {
        return TheTarget.textOf(PageHome.WELCOME_MESSAGE_ENGLISH);
    }

    public static Question<String> welcomeMessageInItalian() {
        return TheTarget.textOf(PageHome.WELCOME_MESSAGE_ITALIAN);
    }

    public static Question<String> usernameLogin() {
        return TheTarget.textOf(PageHome.USERNAME_LOGIN);
    }

    public static Question<String> usernameLoginIncorrect() {
        return TheTarget.textOf(PageHome.USERNAME_LOGIN_INCORRECT);
    }
}
