package questions;

import net.serenitybdd.screenplay.questions.targets.TheTarget;
import ui.PageCarRentingResult;
import net.serenitybdd.screenplay.Question;

public class ThePageCarRentingResults {

    public static Question<String> firstTitleCar() {
        return TheTarget.textOf(PageCarRentingResult.TITLE_FIRST_CAR);
    }
}
