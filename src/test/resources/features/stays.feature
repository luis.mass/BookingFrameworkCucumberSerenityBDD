# language: es
@stays
Característica: Búsqueda de Alojamiento

  Este feature se trata de realizar búsquedas de alojamiento en el sitio de Booking y validar los resultados.


  Esquema del escenario: Buscar resultados relevantes
    Dado que el Tester tiene configurado en la plataforma el idioma español
    Cuando El realiza la búsqueda de: <ciudad>
    Entonces El solo debería poder ver los resultados relacionados con la palabra: <ciudad>

    Ejemplos:
      |ciudad|
      |San Diego, Cartagena de Indias|
      |Laureles - Estadio, Medellín|

