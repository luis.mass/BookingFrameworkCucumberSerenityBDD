# language: es
@cars
Característica: Búsqueda de alquiler de carros

  Este feature se trata de realizar búsquedas de alquiler de autos en el sitio de Booking y validar los resultados.

  Esquema del escenario: Buscar resultados relevantes
    Dado que el Tester va a la sección de alquiler de autos de Booking
    Cuando El realiza la búsqueda de autos disponibles en: <lugar>
    Y ordena los autos de menor a mayor precio
    Entonces debería ver que el auto de menor precio es un Chevrolet Spark

    Ejemplos:
      |lugar|auto|
      |Bogotá|Renault Logan|
      |Medellín|Chevrolet Spark|