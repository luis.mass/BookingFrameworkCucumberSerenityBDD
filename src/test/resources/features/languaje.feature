# language: es

@languaje
Característica: Cambiar lenguaje de la página

  Antecedentes: Ingreso al sitio web de Booking
    Dado que Luis ingresa al sitio web de Booking

  Escenario: Configurar idioma español
    Cuando el configura el idioma en: Español
    Entonces el debería ver el contenido de la página en español

  Escenario: Configurar idioma inglés (US)
    Cuando el configura el idioma en: English (UK)
    Entonces el debería ver el contenido de la página en inglés

  Escenario: Configurar idioma italiano
    Cuando el configura el idioma en: Italiano
    Entonces el debería ver el contenido de la página en italiano

