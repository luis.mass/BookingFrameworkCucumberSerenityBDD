# language: es

@login
Característica: Realizar Login de la aplicación

  Antecedentes: Ingreso al sitio web de Booking
    Dado que Luis tiene una cuenta activa


  Escenario: Acceso exitoso
    Cuando el envía sus credenciales correctas
    Entonces el debería tener acceso al panel principal


  Escenario: Acceso exitoso
    Cuando el envía sus credenciales incorrectas
    Pero el no debería tener acceso al panel principal